<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

use \Workerman\Worker;
use \GatewayWorker\BusinessWorker;
use \Workerman\Autoloader;

// 自动加载类
require_once '../vendor/autoload.php';
require './event/MyEvent.php';
require_once '../extend/MySqli.class.php'; //加载mysqli基本的操作类，封装了一些常用的操作数据库的方法
$conf = include('../extend/mysql.conf.php');
$mysql = new mysql();
$mysql->connect($conf);
$sql = "SELECT * FROM ".$conf['prefix']."plugin_lyz_kefu_chat_option_win";
$list = $mysql->getRow($sql);
$list = json_decode($list['option'],true);
$list = $list['option'];

/*$db = MyPDO::getInstance('','','','','');
$list = $db->fetch("SELECT * FROM ".$conf['prefix']."plugin_lyz_kefu_chat_option_win");
$list = json_decode($list['option'],true);
$list = $list['option'];*/
// bussinessWorker 进程
$worker = new BusinessWorker();
// worker名称
$worker->name = $list['worker_processes_name'];
// bussinessWorker进程数量
$worker->count = $list['worker_processes_num'];
// 服务注册地址
$worker->registerAddress = $list['register_ip'];
/*
 * 设置处理业务的类为MyEvent。
 * 如果类带有命名空间，则需要把命名空间加上，
 * 类似$worker->eventHandler='\my\namespace\MyEvent';
 */
$worker->eventHandler = 'MyEvent';
Worker::$stdoutFile = '../log.txt';
// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    define('GLOBAL_START',1);
    Worker::runAll();
}

