--
-- 表的结构 `cmf_plugin_lyz_kefu_char`
--
CREATE TABLE `cmf_plugin_lyz_kefu_chat_option_win` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `option` text NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='客服系统配置信息表';

INSERT INTO `cmf_plugin_lyz_kefu_chat_option_win` VALUES ('1', '{\"option\":{\"register\":\"text:\\/\\/0.0.0.0:1236\",\"register_ip\":\"127.0.0.1:1236\",\"inner_ip\":\"127.0.0.1\",\"gateway\":\"Websocket:\\/\\/0.0.0.0:7272\",\"gateway_processes_name\":\"mygateway\",\"gateway_processes_num\":\"4\",\"gateway_start_port\":\"2300\",\"pingdata\":\"{&quot;type&quot;:&quot;ping&quot;}\",\"pingInterval\":\"10\",\"worker_processes_name\":\"mybusinessworker\",\"worker_processes_num\":\"4\",\"websocket_processes_name\":\"ws:\\/\\/0.0.0.0:7272\"},\"id\":\"1\",\"_plugin\":\"lyz_kefu_chat\",\"_controller\":\"admin_index\",\"_action\":\"index\"}');
INSERT INTO `cmf_plugin_lyz_kefu_chat_option_win` VALUES ('2', '{\"option\":{\"register\":\"text:\\/\\/0.0.0.0:1236\",\"gateway\":\"Websocket:\\/\\/0.0.0.0:7272\",\"inner_ip\":\"127.0.0.1\",\"gateway_processes_name\":\"mygateway\",\"gateway_processes_num\":\"4\",\"gateway_start_port\":\"2300\",\"pingdata\":\"{&quot;type&quot;:&quot;ping&quot;}\",\"pingInterval\":\"10\",\"worker_processes_name\":\"mybusinessworker\",\"worker_processes_num\":\"4\",\"websocket_processes_name\":\"ws:\\/\\/0.0.0.0:7272\"},\"_plugin\":\"lyz_kefu_chat\",\"_controller\":\"admin_index\",\"_action\":\"index\"}');
