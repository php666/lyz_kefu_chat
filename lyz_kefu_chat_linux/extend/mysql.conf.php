<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/21
 * Time: 15:16
 */
//数据库配置文件
$conf = include('../../../../data/conf/database.php');  //获取thinkcmf的数据库信息
return [
    'host' => $conf['hostname'],
    'user' => $conf['username'],
    'pass' => $conf['password'],
    'dbname' => $conf['database'],
    'dbcharset' => $conf['charset'],
    'prefix' => $conf['prefix'],
    'hostport' => $conf['hostport'],
];