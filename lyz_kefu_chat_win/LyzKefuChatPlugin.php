<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace plugins\lyz_kefu_chat;
use cmf\lib\Plugin;
use think\Db;

//Demo插件英文名，改成你的插件英文就行了
class LyzKefuChatPlugin extends Plugin
{

    public $info = [
        'name'        => 'LyzKefuChat',
        'title'       => '客服系统',
        'description' => '客服系统',
        'status'      => 1,
        'author'      => 'liyuzhao',
        'version'     => '1.0',
        'demo_url'    => 'http://www.liyuzhao.com',
        'author_url'  => 'http://www.liyuzhao.com'
    ];

    public $hasAdmin = 1;//插件是否有后台管理界面

    // 插件安装
    public function install()
    {
        $config = config('database');
        $sql    = cmf_split_sql(PLUGINS_PATH . 'lyz_kefu_chat/data/plugin_lyz_kefu_chat_option_win.sql', $config['prefix'], $config['charset']);
        foreach ($sql as &$value) {Db::execute($value);}
        $sql    = cmf_split_sql(PLUGINS_PATH . 'lyz_kefu_chat/data/plugin_lyz_kefu_chat_user_win.sql', $config['prefix'], $config['charset']);
        foreach ($sql as &$value) {Db::execute($value);}
        $sql    = cmf_split_sql(PLUGINS_PATH . 'lyz_kefu_chat/data/plugin_lyz_kefu_chat_recover_win.sql', $config['prefix'], $config['charset']);
        foreach ($sql as &$value) {Db::execute($value);}
        return true;//安装成功返回true，失败false
    }

    // 插件卸载
    public function uninstall()
    {
        $config = config('database');
        Db::execute('DROP TABLE ' . $config['prefix'] . 'plugin_lyz_kefu_chat_option_win');
        Db::execute('DROP TABLE ' . $config['prefix'] . 'plugin_lyz_kefu_chat_user_win');
        Db::execute('DROP TABLE ' . $config['prefix'] . 'plugin_lyz_kefu_chat_recover_win');
        return true;//卸载成功返回true，失败false
    }

    //实现的footer_start钩子方法
    public function footerStart($param)
    {
        $config = $this->getConfig();
        $this->assign($config);
        echo $this->fetch('widget');
    }

}