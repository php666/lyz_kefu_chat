CREATE TABLE `cmf_plugin_lyz_kefu_chat_recover_win` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL COMMENT '用户名称',
  `kefu_id` int(11) NOT NULL,
  `kefu_name` varchar(100) NOT NULL COMMENT '客服名称',
  `room_id` bigint(16) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户是否还在线（1 在线 -1 不在线）',
  `create_time` int(11) NOT NULL COMMENT '数据添加的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户呼叫客服记录表';

