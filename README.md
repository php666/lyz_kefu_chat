# lyz_kefu_chat
基于thinkcmf5的一个客服聊天插件，该插件采用了workerman。该插件只能在win环境下运行。

 **步骤：
1：如果是win环境下载lyz_kefu_chat_win 这个文件，然后修改文件名为lyz_kefu_chat。然后放到thinkcmf5的插件文件夹中，打开后台安装插件。

2：如果是linux环境下载lyz_kefu_chat_linux 这个文件，然后修改文件名为lyz_kefu_chat。然后放到thinkcmf5的插件文件夹中，打开后台安装插件。

3:开启服务。win环境的服务开启，直接双击打开插件文件lyz_kefu_chat/start/start_for_win.bat的文件，服务自动开启
linux环境的服务开启，在linux下打开终端，cd到lyz_kefu_chat/start/这个插件目录，在终端输入 php start_for_linux.php start或者php start_for_linux.php start -d。-d的意思是进程可以持久化，关闭终端workman的服务进程并没有关闭。

4:打开前台页面就会看到插件的入口，点击进入即可。

5:不懂的可以留言，如果你觉得这个插件还可以，希望你能给我个star

6:此插件会一直更新，如果有想法的欢迎联系我，我扣扣562405704.邮箱：562405704@qq.com 

7:欢迎大家到我的博客转转(http://www.liyuzhao.cn)** 

问题：
1：如果linux下打开服务出现Please install posix extension. See http://doc3.workerman.net/appendices/install-extension.html

解决方法：
1.安装php-posix
#yum -y install php-process  //这里要注意一下，我的php版本是5.6的，所以这里要改为#yum -y install php56w-process
2.验证是否安装上了
#php -m|grep posix
posix

 **演示地址： http://www.spcmf.cn/sp/public/index.php/plugin/lyz_kefu_chat/index/chat.html** 